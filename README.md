# elm-canvas sketch

This is an example sketch for the [elm-canvas](https://package.elm-lang.org/packages/joakin/elm-canvas/latest/) library.

# To run

```bash
./start.sh
```

```bash
elm-live src/Main.elm -d public -o -- --output=public/elm.js
```

