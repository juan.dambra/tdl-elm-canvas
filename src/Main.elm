module Main exposing (main)

-- import Json.Decode as Decode

import Browser
import Browser.Events exposing (onAnimationFrameDelta, onKeyDown)
import Browser.Navigation exposing (Key)
import Canvas exposing (..)
import Canvas.Settings exposing (..)
import Canvas.Settings.Advanced exposing (..)
import Canvas.Texture exposing (sprite)
import Color
import Debug as Console
import Element.Font as Font
import Html exposing (Html, div, li, object, p, text, ul)
import Html.Attributes exposing (style)
import Json.Decode exposing (null)
import Keyboard exposing (Key(..))
import Keyboard.Arrows
import Style
import Color


type alias Model =
    { count : Float
    , debug : Bool
    , player1 : Object
    , player2 : Object
    , ball : Object
    , pressedKeys : List Key
    , gameStatus : GameStatus
    }


type alias Object =
    { position : Point
    , velocity : Point
    , inFloor : Bool
    , name : String
    , score : Int
    }


type alias GameStatus =
    { gameWinner : Maybe Object
    , lastPointWinner : Maybe Object
    }



slimeSize =
    100


ballSize =
    30


gameLimit =
    1


type alias Borders =
    { x0 : Float
    , x1 : Float
    , y0 : Float
    }


slimeBorders : Float -> Float -> Borders
slimeBorders size right =
    { x0 = size + right * width / 2
    , x1 = width - size - (1 - right) * width / 2
    , y0 = height - size
    }


ballBorders : Float -> Borders
ballBorders size =
    { x0 = size
    , x1 = width - size
    , y0 = height - 100 - size
    }


constants =
    { baseSpeedX = 2.5
    , baseSpeedY =
        1.0
    , jumpSpeed = 15
    , borders =
        { slime1 = slimeBorders (toFloat slimeSize) 0
        , slime2 = slimeBorders (toFloat slimeSize) 1
        , ball = ballBorders (toFloat ballSize)
        }
    , initialBallSpeed = ( -5, 10 )
    }


type Msg
    = Frame Float
    | KeyboardMsg Keyboard.Msg


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


width : Float
width =
    800


height : Float
height =
    600


heightBall =
    150


centerX : Float
centerX =
    width / 2


centerY : Float
centerY =
    height


type Zone
    = Left
    | Right
    | Top
    | Else


inZone pos =
    let
        ( x, y ) =
            pos

        halfBall =
            ballSize

        ( leftNet, rightNet ) =
            ( centerX - halfBall, centerX + halfBall )

        topNet =
            350
    in
    if y > topNet then
        if x > leftNet && x < centerX then
            Left

        else if x > centerX && x < rightNet then
            Right

        else
            Else

    else
        Else


netCheckPos pos =
    let
        ( x, y ) =
            pos

        ( leftNet, rightNet ) =
            ( centerX - ballSize, centerX + ballSize )

        topNet =
            350
    in
    case inZone pos of
        Left ->
            -- Debug.log "Left"
            ( leftNet, y )

        Right ->
            -- Debug.log "Right"
            ( rightNet, y )

        Top ->
            ( x, topNet )

        Else ->
            ( x, y )


getBorders object =
    case object.name of
        "Player1" ->
            constants.borders.slime1

        "Player2" ->
            constants.borders.slime2

        "Ball" ->
            constants.borders.ball

        _ ->
            constants.borders.ball


updateSlimeScore : Object -> Object -> Int
updateSlimeScore slime ball =
    let
        ( bx, by ) =
            ball.position

        score =
            case slime.name of
                "Player1" ->
                    if inFloor ball && (bx > centerX) then
                        slime.score + 1

                    else
                        slime.score

                "Player2" ->
                    if inFloor ball && (bx < centerX) then
                        slime.score + 1

                    else
                        slime.score

                _ ->
                    slime.score
    in
    score


updatePositionSlime : Object -> Object -> Point
updatePositionSlime slime ball =
    let
        ( x, y ) =
            slime.position

        ( vX, vY ) =
            slime.velocity

        centerRatio =
            if slime.name == "Player1" then
                -150

            else
                150

        ( nextX, nextY ) =
            if inFloor ball then
                ( centerX + centerRatio, centerY )

            else
                ( x + vX, y - vY )
                    |> borderCheckXY (getBorders slime)

        position =
            ( nextX, nextY )
    in
    position


borderCheckX objectBorder pos =
    min (max objectBorder.x0 pos) objectBorder.x1


borderCheckY objectBorder pos =
    min pos objectBorder.y0


borderCheckXY objectBorder pos =
    ( borderCheckX objectBorder (Tuple.first pos), borderCheckY objectBorder (Tuple.second pos) )


ballCheckXY slimePos ballPos =
    let
        ( sX, sY ) =
            slimePos

        ( bX, bY ) =
            ballPos

        ( cx, cy ) =
            ( sX - bX, sY - bY )

        h =
            sqrt (cx ^ 2 + cy ^ 2)

        minH =
            ballSize + slimeSize

        angulo =
            atan2 cy cx
    in
    if h > minH then
        ballPos

    else
        ( sX - cos angulo * minH, sY - sin angulo * minH )



--(sX + ballSize + slimeSize)


updateBallScore : Object -> Int
updateBallScore ball =
    let
        score =
            if inFloor ball then
                ball.score + 1

            else
                ball.score
    in
    score


updatePositionBall : Object -> Object -> Object -> Point
updatePositionBall ball slime slime2 =
    let
        ( x, y ) =
            ball.position

        ( sX, sY ) =
            slime.position

        ( vX, vY ) =
            ball.velocity

        -- nextX =
        --     (x + vX)
        --         |> borderCheckX constants.borders.ball
        --         -- |> ballCheckX slime.position
        -- nextY =
        --     borderCheckY constants.borders.ball (y - vY)
        --     -- |> ballCheckY slime.position
        ( nextX, nextY ) =
            if inFloor ball then
                ( centerX, heightBall )

            else
                ( x + vX, y - vY )
                    |> borderCheckXY (getBorders ball)
                    |> ballCheckXY slime.position
                    |> ballCheckXY slime2.position
                    |> netCheckPos

        position =
            ( nextX, nextY )
    in
    position


inFloor : Object -> Bool
inFloor slime =
    let
        ( _, y ) =
            slime.position
    in
    if y == (getBorders slime).y0 then
        True

    else
        False


inFloorBall : Object -> Bool
inFloorBall ball =
    let
        ( _, y ) =
            ball.position
    in
    if y == (getBorders ball).y0 then
        True

    else
        False


inWallBall : Object -> Bool
inWallBall ball =
    let
        ( x, _ ) =
            ball.position

        ( vX, _ ) =
            ball.velocity

        nextx =
            x + vX
    in
    if nextx <= (getBorders ball).x0 then
        True

    else if nextx >= (getBorders ball).x1 then
        True

    else
        False


updateVelocitySlime : Object -> Point
updateVelocitySlime slime =
    let
        ( modelVX, modelVY ) =
            slime.velocity

        speedY =
            if inFloor slime then
                max 0 modelVY

            else
                modelVY - constants.baseSpeedY

        speedX =
            modelVX
    in
    ( speedX, speedY )


floorCheck ball modelV =
    let
        newY =
            if inFloorBall ball then
                bounce (Tuple.second modelV)

            else
                Tuple.second modelV - constants.baseSpeedY

        newX =
            Tuple.first modelV
    in
    ( newX, newY )


wallCheck ball modelV =
    let
        newX =
            if inWallBall ball then
                -1 * Tuple.first modelV

            else
                Tuple.first modelV

        newY =
            Tuple.second modelV
    in
    ( newX, newY )


slimeCheck ball slime vel =
    let
        ( sX, sY ) =
            slime.position

        ( bX, bY ) =
            ball.position

        ( vX, vY ) =
            ball.velocity

        velModule =
            sqrt (vX ^ 2 + vY ^ 2)

        ( cx, cy ) =
            ( sX - bX, sY - bY )

        h =
            sqrt (cx ^ 2 + cy ^ 2)

        minH =
            ballSize + slimeSize

        angulo =
            atan2 cy cx

        ( newX, newY ) =
            if h > minH then
                vel

            else
                ( cos angulo * -velModule, sin angulo * velModule )
    in
    ( newX, newY )


maxVel vel =
    let
        ( vx, vy ) =
            vel
    in
    ( min vx 25, min vy 25 )


inNetBallTop ball =
    let
        ( posX, posY ) =
            ball.position

        netWidth =
            30
    in
    if (posX > centerX - netWidth && posX < centerX + netWidth) && (posY > 300 && posY < 400) then
        True

    else
        False


inNetBallSide ball =
    let
        ( posX, posY ) =
            ball.position

        ( vX, vY ) =
            ball.velocity

        ( nextX, nextY ) =
            ( posX + vX, posY + vY )

        netWidth =
            ballSize

        netTop =
            350
    in
    if (nextX >= centerX - netWidth && nextX <= centerX + netWidth) && nextY > netTop then
        True

    else
        False


netCheckVel ball modelV =
    let
        newY =
            if inNetBallTop ball then
                bounce (Tuple.second modelV)

            else
                Tuple.second modelV

        newX =
            if inNetBallSide ball then
                -(Tuple.first modelV)

            else
                Tuple.first modelV
    in
    ( newX, newY )


updateVelocityBall : Object -> Object -> Object -> Maybe Object -> Point
updateVelocityBall ball slime1 slime2 gameWinner =
    case gameWinner of
        Nothing ->
            if inFloor ball then
                constants.initialBallSpeed

            else
                ball.velocity
                    |> floorCheck ball
                    |> wallCheck ball
                    |> slimeCheck ball slime1
                    |> slimeCheck ball slime2
                    |> netCheckVel ball
                    |> maxVel
        
        Just _ ->
            (0, 0)


bounce vel =
    if vel == 0 then
        vel

    else
        abs vel - 3


updateBall : Object -> Object -> Object -> Model-> Object
updateBall ball slime1 slime2 model =
    { ball | velocity = updateVelocityBall ball slime1 slime2 model.gameStatus.gameWinner, position = updatePositionBall ball slime1 slime2, score = updateBallScore ball }


updateSlime slime ball =
    { slime | velocity = updateVelocitySlime slime, position = updatePositionSlime slime ball, score = updateSlimeScore slime ball }


updateGameStatus : Object -> Object -> Object -> GameStatus -> GameStatus
updateGameStatus ball player1 player2 actualGameStatus =
    let
        ( bx, _ ) =
            ball.position
    in
    if inFloor ball then
        if bx > centerX then
            { actualGameStatus | lastPointWinner = Just player1 }

        else
            { actualGameStatus | lastPointWinner = Just player2 }

    else
        actualGameStatus


updateGameWinner : Object -> Object -> GameStatus -> GameStatus
updateGameWinner player1 player2 actualGameStatus =
    let
        gameWinner =
            if player1.score == gameLimit || player2.score == gameLimit then
                if player1.score > player2.score then
                    Maybe.Just player1

                else
                    Maybe.Just player2

            else
                Maybe.Nothing
    in
    case gameWinner of
        Nothing ->
            actualGameStatus

        Just winner ->
            { actualGameStatus | gameWinner = Just winner }


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Frame _ ->
            let
                player1 =
                    updateSlime model.player1 ball

                player2 =
                    updateSlime model.player2 ball

                ball =
                    updateBall model.ball model.player1 model.player2 model

                newGameStatus =
                    updateGameStatus model.ball model.player1 model.player2 model.gameStatus

                newGameWinner =
                    updateGameWinner player1 player2 newGameStatus
            in
            ( -- Debug.log (Debug.toString position)
              { model
                | count = model.count + 1
                , player1 = player1
                , player2 = player2
                , ball = ball
                , gameStatus = newGameWinner
              }
            , Cmd.none
            )

        KeyboardMsg keyMsg ->
            let
                ( pressedKeys, maybeKeyChange ) =
                    Keyboard.updateWithKeyChange Keyboard.anyKeyOriginal keyMsg model.pressedKeys

                debug =
                    case maybeKeyChange of
                        Just (Keyboard.KeyDown (Character "L")) ->
                            not model.debug

                        _ ->
                            model.debug

                jump1 =
                    case maybeKeyChange of
                        Just (Keyboard.KeyDown ArrowUp) ->
                            True

                        _ ->
                            False

                jump2 =
                    case maybeKeyChange of
                        Just (Keyboard.KeyDown (Character "w")) ->
                            True

                        _ ->
                            False

                arrows =
                    Keyboard.Arrows.arrows pressedKeys

                letters =
                    if List.member (Character "a") pressedKeys then
                        { x = -1, y = 0 }

                    else if List.member (Character "d") pressedKeys then
                        { x = 1, y = 0 }

                    else
                        { x = 0, y = 0 }

                -- case pressedKeys of
                --     List.member Character "A" ->
                --         {x = -1 , y = 0 }
                --     Just (Keyboard.KeyDown (Character "D")) ->
                --         {x = 1 , y = 0 }
                --     _ ->
                --         {x = 0 , y = 0 }
                speedMultiplier =
                    3

                ( _, modelVY1 ) =
                    player1.velocity

                ( _, modelVY2 ) =
                    player2.velocity

                speedY1 =
                    if inFloor player1 && jump1 then
                        constants.jumpSpeed

                    else
                        modelVY1

                speedY2 =
                    if inFloor player2 && jump2 then
                        constants.jumpSpeed

                    else
                        modelVY2

                speedX1 =
                    toFloat arrows.x * constants.baseSpeedX * speedMultiplier

                speedX2 =
                    toFloat letters.x * constants.baseSpeedX * speedMultiplier

                player1 =
                    model.player1

                player2 =
                    model.player2
            in
            ( { model
                | pressedKeys = pressedKeys
                , debug = debug
                , player1 = { player1 | velocity = ( speedX1, speedY1 ) }
                , player2 = { player2 | velocity = ( speedX2, speedY2 ) }
              }
            , Cmd.none
            )


init : () -> ( Model, Cmd Msg )
init _ =
    let
        position1 =
            ( centerX - 150
            , centerY
            )

        position2 =
            ( centerX + 150
            , centerY
            )

        positionBall =
            ( centerX
            , heightBall
            )

        velocity1 =
            ( 0.0, 0.0 )

        player1 =
            { position = position1
            , velocity = velocity1
            , inFloor = False
            , name = "Player1"
            , score = 0
            }

        player2 =
            { position = position2
            , velocity = velocity1
            , inFloor = False
            , name = "Player2"
            , score = 0
            }

        ball =
            { position = positionBall
            , velocity = constants.initialBallSpeed
            , inFloor = False
            , name = "Ball"
            , score = 0
            }

        initialGameStatus =
            { gameWinner = Nothing
            , lastPointWinner = Nothing
            }
    in
    ( { count = 0
      , ball = ball
      , player1 = player1
      , player2 = player2
      , pressedKeys = []
      , debug = False
      , gameStatus = initialGameStatus
      }
    , Cmd.none
    )



-- SUB


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Sub.map KeyboardMsg Keyboard.subscriptions
        , onAnimationFrameDelta Frame
        ]


view : Model -> Html Msg
view model =
    div
        [ style "display" "flex"
        , style "justify-content" "center"
        , style "align-items" "center"
        ]
        [ renderScore model
        , Canvas.toHtml
            ( round width, round height )
            [ style "border" "10px solid rgba(0,0,0,0.1)" ]
            [ clearScreen
            , renderSlimePlayerOne model model.player1
            , renderSlimePlayerTwo model model.player2
            , renderBall model model.ball
            , renderFloor
            , renderNet model
            , renderWinnerHat model
            ]
        , if model.debug then
            renderDebug model

          else
            p [] []
        ]

winnerHat : Point -> Shape
winnerHat (posX, posY) = 
    let
        (fixX, fixY) = (posX, posY - slimeSize - 100)
    in
    path ( fixX, fixY )
        [ lineTo ( fixX - 40, fixY + 100 )
        , lineTo ( fixX + 40, fixY + 100 )
        , lineTo ( fixX, fixY )
        ]

renderWinnerHat : Model -> Renderable
renderWinnerHat { gameStatus } =
    case gameStatus.gameWinner of
        Nothing ->
            shapes [] []

        Just winner ->
            shapes [fill Color.darkYellow] [winnerHat winner.position]

renderScore : Model -> Html Msg
renderScore { player1, player2, gameStatus } =
    case gameStatus.gameWinner of
        Nothing ->
            case gameStatus.lastPointWinner of
                Nothing ->
                    div Style.score
                        [ renderInitialMessage
                        ]

                Just pointWinner ->
                    div Style.score
                        [ renderPlayerPoints player1 player2
                        , renderLastPointWinner pointWinner.name
                        ]

        Just gameWinner ->
            div Style.score
                [ renderGameWinner gameWinner.name
                ]


renderDebug : Model -> Html Msg
renderDebug model =
    let
        shiftPressed =
            List.member Shift model.pressedKeys

        arrows =
            Keyboard.Arrows.arrows model.pressedKeys
    in
    div Style.container
        [ p [] [ renderObjectDebug model.ball ]
        , p [] [ renderObjectDebug model.player1 ]
        , p [] [ Html.text (Debug.toString model.pressedKeys) ]
        , p [] [ Html.text (Debug.toString (inFloor model.ball)) ]
        , p [] [ Html.text (Debug.toString (inFloor model.player1)) ]

        -- p [] [ Html.text (Debug.toString model.ball) ]
        -- , p [] [ Html.text ("Shift: " ++ Debug.toString shiftPressed) ]
        -- , p [] [ Html.text ("Arrows: " ++ Debug.toString arrows) ]
        -- , p [] [ Html.text ("Position: X:" ++ Debug.toString model.posX ++ " Y:" ++ Debug.toString model.posY) ]
        -- , p [] [ Html.text ("Center: X:" ++ Debug.toString centerX ++ " Y:" ++ Debug.toString centerY) ]
        -- , p [] [ Html.text "Currently pressed down:" ]
        -- , ul []
        -- (List.map (\key -> li [] [ Html.text (Debug.toString key) ]) model.pressedKeys)
        ]


renderObjectDebug : Object -> Html msg
renderObjectDebug obj =
    -- Debug.log
    --     (Debug.toString
    --         obj
    --     )
    p
        []
        [ p [] [ Html.text (Debug.toString obj.velocity) ]
        , p [] [ Html.text (Debug.toString obj.position) ]
        , p [] [ Html.text (Debug.toString obj.score) ]
        ]


renderInitialMessage : Html msg
renderInitialMessage =
    p
        []
        [ Html.h4 [] [ Html.text "Welcome to Elm Volley!" ]
        , p [] [ Html.text "Player 1 uses Arrows keys to move" ]
        , p [] [ Html.text "Player 2 uses WASD keys to move" ]
        ]


renderPlayerPoints : Object -> Object -> Html msg
renderPlayerPoints player1 player2 =
    -- Debug.log
    --     (Debug.toString
    --         obj
    --     )
    p
        []
        [ p [] [ Html.text ("Score of Player 1: " ++ String.fromInt player1.score) ]
        , p [] [ Html.text ("Score of Player 2: " ++ String.fromInt player2.score) ]
        ]


renderLastPointWinner : String -> Html msg
renderLastPointWinner name =
    p []
        [ p [] [ Html.text ("Winner of last point was: " ++ name) ]
        ]


renderGameWinner : String -> Html msg
renderGameWinner name =
    p []
        [ p [] [ Html.text ("Game is over! The winner is " ++ name ++ ". Thank you for playing!") ]
        ]


clearScreen : Renderable
clearScreen =
    shapes [ fill Color.white ] [ rect ( 0, 0 ) width height ]


renderFloor : Renderable
renderFloor =
    shapes [ fill Color.green ] [ rect ( 0, height - 100 ) width 100 ]


renderASlime : Color.Color -> Point -> Float -> Renderable
renderASlime color position size =
    shapes
        [ transform
            [ translate 0 0
            ]
        , fill color
        ]
        [ arc position size { startAngle = degrees 180, endAngle = degrees 0, clockwise = True }
        ]

renderADeadSlime : Color.Color -> Point -> Float -> Renderable
renderADeadSlime color position size =
    let
        (x, y) = position

        newPosition = (x, y - size)
    in
    shapes
        [ transform
            [ translate 0 0
            ]
        , fill color
        ]
        [ 
            rect newPosition 25 size,
            rect (x - 37, y - (size - 25)) size 25
        ]


renderSlimePlayerOne : Model -> Object -> Renderable
renderSlimePlayerOne model player =
    let
        size =
            slimeSize
        color = Color.hsl (degrees (model.count / 4)) 0.3 0.7

        pos = player.position

    in
    case model.gameStatus.gameWinner of
        Nothing ->
            renderASlime color pos size
        
        Just winner ->
            if player.name == winner.name then
                renderASlime color pos size
            else
                renderADeadSlime color pos size



renderSlimePlayerTwo : Model -> Object -> Renderable
renderSlimePlayerTwo model player =
    let
        size =
            slimeSize
        color = Color.blue

        pos = player.position
    in
    case model.gameStatus.gameWinner of
    Nothing ->
        renderASlime color pos size
    
    Just winner ->
        if player.name == winner.name then
            renderASlime color pos size
        else
            renderADeadSlime color pos size


renderBall : Model -> Object -> Renderable
renderBall model ball =
    let
        size =
            ballSize
    in
    shapes
        [ transform
            [ translate 0 0
            ]
        , fill Color.black
        ]
        [ circle ball.position size
        ]


renderNet : Model -> Renderable
renderNet model =
    let
        size =
            ballSize

        netHeight =
            150
    in
    shapes
        [ transform
            [ translate 0 0
            ]
        , fill Color.black
        ]
        [ rect ( centerX, 500 - netHeight ) 10 netHeight
        ]