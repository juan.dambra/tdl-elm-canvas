module Style exposing (container, score)

import Html
import Html.Attributes exposing (style)


container : List (Html.Attribute msg)
container =
    [ style "background-color" "rebeccapurple"
    , style "color" "white"
    , style "font-family" "sans-serif"
    , style "width" "20vw"
    , style "height" "70vh"
    , style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "flex-start"
    , style "box-sizing" "border-box"
    , style "padding" "4em 2em"
    , style "flex-direction" "column"
    ]
score : List (Html.Attribute msg)
score =
    [ style "background-color" "orange"
    , style "color" "black"
    , style "font-family" "sans-serif"
    , style "width" "auto"
    , style "height" "auto"
    , style "display" "flex"
    , style "align-items" "center"
    , style "justify-content" "flex-start"
    , style "box-sizing" "border-box"
    , style "padding" "4em 2em"
    , style "flex-direction" "column"
    ]